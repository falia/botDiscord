const Discord = require("discord.js");
const myBot = new Discord.Client();
const config = require("./config.json");

// Set playing
myBot.on('ready', function () {
    console.log("Je suis là !");
    myBot.user.setGame("Kingdom Heart III");
})

let prefix = config.prefix;

// message Event
myBot.on('message', message => {
    
    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // prevent from answering to others bots
    if(!command || message.author.bot) return ;

    // send a direct message with all the available commands
    if(command === "help"){
        let author = message.author;
        author.send('Not available yet');
    }

    // tell a random joke
    if(command === "joke"){

        // jokeArray
        require("./joke.js");

        // get a random number of an array
        function random(arr){
            return arr[Math.floor(Math.random() * arr.length)];
        }

        let theJoke = random(jokeArray);

        message.channel.send(theJoke);
    }

    // only owner command
    if(message.author.id !== config.OwnerID) return;

    // Speak instead of the bot
    // idGuild or channel||message
    if(command === 'say'){

        // get the message
        let sayMessage = args.join(" ");
        
        let fields = sayMessage.split('||');
        let guildID = fields[0];
        let message = fields[1];

        myBot.channels.get(guildID).send(message);
    }


});

// new member joined Event
myBot.on('guildMemberAdd', member => {
    const guild = member.guild;
    guild.defaultChannel.send(`Bienvenue à , ${member}, sur le serveur`);
});

myBot.login(config.token);